#include "PIC1257x_Prog.h"

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

PIC1257x_Prog::PIC1257x_Prog(int mclr_pin, int pgd_pin, int pgc_pin) :
  mclr_pin_(mclr_pin), pgd_pin_(pgd_pin), pgc_pin_(pgc_pin) {}

void PIC1257x_Prog::WriteBit(bool value) {
  pinMode(pgd_pin_, OUTPUT);
  digitalWrite(pgd_pin_, value);
  digitalWrite(pgc_pin_, 1);
  delayMicroseconds(1);
  digitalWrite(pgc_pin_, 0);
  delayMicroseconds(1);
}

bool PIC1257x_Prog::ReadBit() {
  pinMode(pgd_pin_, INPUT_PULLUP);
  digitalWrite(pgc_pin_, 1);
  delayMicroseconds(1);
  digitalWrite(pgc_pin_, 0);
  bool result = digitalRead(pgd_pin_);
  delayMicroseconds(1);
  return result;
}

void PIC1257x_Prog::Write(uint32_t value, unsigned count) {
  while (count--) {
    WriteBit(value & 1);
    value >>= 1;
  }
}

uint32_t PIC1257x_Prog::Read(unsigned count) {
  uint32_t result = 0;
  for (unsigned i = 0; i < count; ++i) {
    result |= ReadBit() << i;
  }
  return result;
}

void PIC1257x_Prog::WriteCommand(Command cmd) {
  Write((uint32_t) cmd, 6);
  delayMicroseconds(1);
}

void PIC1257x_Prog::WriteArg(uint16_t arg) {
  arg = (arg & 0x3FFF) << 1;
  Write(arg, 16);
}

uint16_t PIC1257x_Prog::ReadArg() {
  uint16_t arg = Read(16);
  return (arg >> 1) & 0x3FFF;
}

void PIC1257x_Prog::EnterProgramming() {
  delayMicroseconds(1);
  pinMode(pgc_pin_, OUTPUT);
  pinMode(mclr_pin_, OUTPUT);
  digitalWrite(pgc_pin_, 0);
  digitalWrite(mclr_pin_, 0);
  delayMicroseconds(20);
  Write(0x4D434850, 32);
  WriteBit(false);
  delay(1);
}

void PIC1257x_Prog::ExitProgramming() {
  pinMode(pgd_pin_, INPUT);
  pinMode(pgc_pin_, INPUT);
  pinMode(mclr_pin_, INPUT);
}

void PIC1257x_Prog::LoadConfiguration(uint16_t value) {
  WriteCommand(kLoadConfiguration);
  WriteArg(value);
}

void PIC1257x_Prog::LoadDataForProgMem(uint16_t value) {
  WriteCommand(kLoadDataForProgMem);
  WriteArg(value);
}

uint16_t PIC1257x_Prog::ReadDataFromProgMem() {
  WriteCommand(kReadDataFromProgMem);
  return ReadArg();
}

void PIC1257x_Prog::IncrementAddress(unsigned count) {
  for (unsigned i = 0; i < count; ++i) {
    WriteCommand(kIncrementAddress);
  }
}

void PIC1257x_Prog::ResetAddress() {
  WriteCommand(kResetAddress);
}

void PIC1257x_Prog::BeginIntProgramming() {
  WriteCommand(kBeginIntProgramming);
}

void PIC1257x_Prog::BeginExtProgramming() {
  WriteCommand(kBeginExtProgramming);
}

void PIC1257x_Prog::EndExtProgramming() {
  WriteCommand(kEndExtProgramming);
}

void PIC1257x_Prog::BulkEraseProgMem() {
  WriteCommand(kBulkEraseProgMem);
}

void PIC1257x_Prog::RowEraseProgMem() {
  WriteCommand(kRowEraseProgMem);
}

PIC1257x_Prog::DeviceId PIC1257x_Prog::GetDeviceId() {
  DeviceId result;

  // Jump to address 0x8005 (revision ID register).
  LoadConfiguration();
  IncrementAddress(5);
  result.revision_id = ReadDataFromProgMem();

  // Jump to address 0x8006 (device ID register).
  IncrementAddress();
  result.device_id = ReadDataFromProgMem();

  return result;
}

void PIC1257x_Prog::BulkErase(bool with_user) {
  if (with_user) {
    LoadConfiguration();
  } else {
    ResetAddress();
  }

  BulkEraseProgMem();
  delay(5);
}

void PIC1257x_Prog::EraseUserId() {
  LoadConfiguration();
  RowEraseProgMem();
  delay(3);
}

void PIC1257x_Prog::WriteProgram(InputArray const &data) {
  size_t const size = data.size();
  size_t address = 0;
  ResetAddress();
  while (address < size) {
    size_t towrite = size - address;
    if (towrite > 16) towrite = 16;
    for (int i = 0; i < towrite; ++i) {
      LoadDataForProgMem(data[address++]);
      if (i == towrite - 1) {
        BeginIntProgramming();
        delay(3);
      }
      IncrementAddress();
    }
  }
}

bool PIC1257x_Prog::VerifyProgram(InputArray const &data) {
  size_t const size = data.size();
  size_t address = 0;
  ResetAddress();
  while (address < size) {
    uint16_t actual = ReadDataFromProgMem();
    if (actual != data[address++]) return false;
    IncrementAddress();
  }
  return true;
}

void PIC1257x_Prog::ReadProgram(uint16_t * result, size_t size) {
  ResetAddress();
  while (size--) {
    *result++ = ReadDataFromProgMem();
    IncrementAddress();
  }
}

void PIC1257x_Prog::WriteUserAndConfig(InputArray const *user, InputArray const *config) {
  LoadConfiguration();
  for (int i = 0; i < 4; ++i) {
    if (user) {
      LoadDataForProgMem((*user)[i]);
      BeginIntProgramming();
      delay(5);
    }
    IncrementAddress();
  }
  IncrementAddress(3);
  for (int i = 0; i < 2; ++i) {
    if (config) {
      LoadDataForProgMem((*config)[i]);
      BeginIntProgramming();
      delay(5);
    }
    IncrementAddress();
  }
}

bool PIC1257x_Prog::VerifyUserAndConfig(InputArray const *user, InputArray const *config) {
  LoadConfiguration();
  for (int i = 0; i < 4; ++i) {
    if (user && ReadDataFromProgMem() != (*user)[i]) return false;
    IncrementAddress();
  }
  IncrementAddress(3);
  for (int i = 0; i < 2; ++i) {
    if (config && ReadDataFromProgMem() != (*config)[i]) return false;
    IncrementAddress();
  }
  return true;
}

void PIC1257x_Prog::ReadUserAndConfig(uint16_t * user, uint16_t * config) {
  LoadConfiguration();
  for (int i = 0; i < 4; ++i) {
    if (user) *user++ = ReadDataFromProgMem();
    IncrementAddress();
  }
  IncrementAddress(3);
  for (int i = 0; i < 2; ++i) {
    if (config) *config++ = ReadDataFromProgMem();
    IncrementAddress();
  }
}

