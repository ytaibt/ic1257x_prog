#pragma once

#include <stddef.h>
#include <stdint.h>

#include <avr/pgmspace.h>

class PIC1257x_Prog {
 public:
  struct DeviceId {
    uint16_t device_id;
    uint16_t revision_id;
  };

  class InputArray {
   public:
    virtual size_t size() const = 0;
    virtual uint16_t operator[](size_t index) const = 0;
  };

  class RamInputArray : public InputArray {
   public:
    RamInputArray(uint16_t const * data, size_t size) : data_(data), size_(size) {}
    virtual size_t size() const { return size_; }
    virtual uint16_t operator[](size_t index) const { return data_[index]; }

   private:
    uint16_t const * const data_;
    size_t const size_;
  };

  class FlashInputArray : public InputArray {
    public:
    FlashInputArray(uint16_t data_addr, size_t size) : data_addr_(data_addr), size_(size) {}
    virtual size_t size() const { return size_; }
    virtual uint16_t operator[](size_t index) const { return pgm_read_word(data_addr_ + index * 2); }

   private:
    uint16_t const data_addr_;
    size_t const size_;
  };

  static uint16_t const kDeviceIdPIC12F1571 = 0x3051;
  static uint16_t const kDeviceIdPIC12LF1571 = 0x3053;
  static uint16_t const kDeviceIdPIC12F1572 = 0x3050;
  static uint16_t const kDeviceIdPIC12LF1572 = 0x3052;

  PIC1257x_Prog(int mclr_pin, int pgd_pin, int pgc_pin);

  void EnterProgramming();
  void ExitProgramming();
  DeviceId GetDeviceId();

  void BulkErase(bool with_user = false);
  void EraseUserId();

  void WriteProgram(InputArray const &data);
  bool VerifyProgram(InputArray const &data);
  void ReadProgram(uint16_t * result, size_t size);

  void WriteUserAndConfig(InputArray const *user, InputArray const *config);
  bool VerifyUserAndConfig(InputArray const *user, InputArray const *config);
  void ReadUserAndConfig(uint16_t * user, uint16_t * config);

 private:
  enum Command {
    kLoadConfiguration = 0x00,
    kLoadDataForProgMem = 0x02,
    kReadDataFromProgMem = 0x04,
    kIncrementAddress = 0x06,
    kResetAddress = 0x16,
    kBeginIntProgramming = 0x08,
    kBeginExtProgramming = 0x18,
    kEndExtProgramming = 0x0A,
    kBulkEraseProgMem = 0x09,
    kRowEraseProgMem = 0x11,
  };

  int const mclr_pin_;
  int const pgd_pin_;
  int const pgc_pin_;

  void WriteBit(bool value);
  bool ReadBit();

  void Write(uint32_t value, unsigned count);
  uint32_t Read(unsigned count);

  void WriteCommand(Command cmd);
  void WriteArg(uint16_t arg);
  uint16_t ReadArg();

  void LoadConfiguration(uint16_t value = 0);
  void LoadDataForProgMem(uint16_t value);
  uint16_t ReadDataFromProgMem();
  void IncrementAddress(unsigned count = 1);
  void ResetAddress();
  void BeginIntProgramming();
  void BeginExtProgramming();
  void EndExtProgramming();
  void BulkEraseProgMem();
  void RowEraseProgMem();
};

